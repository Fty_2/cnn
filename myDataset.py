"""
Fix Me: define your own dataloader with the function: get_dataloader
"""
import torch
from torch.utils.data import Dataset
import numpy as np
import uproot

from config import args

def get_dataloader(loaderType, data_slice_id, num_slices, data_size, batch_size):
    if loaderType=='train':
        # Train data loader
        trainset = MyDataset("train", data_slice_id, num_slices, data_size)
        loader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True,
                                        pin_memory=True)
    elif loaderType=='test':
        # Test data loader
        testset = MyDataset("test", data_slice_id, num_slices, data_size)
        loader = torch.utils.data.DataLoader(testset, batch_size=batch_size, shuffle=False,
                                        pin_memory=True)   
    elif loaderType=='apply':
        # Apply data loader
        applyset = MyDataset("apply", data_slice_id, num_slices, data_size)
        loader = torch.utils.data.DataLoader(applyset, batch_size=batch_size, shuffle=False)   

    return loader




# Define your own dataset
import torchvision
import torchvision.transforms as transforms
class MyDataset(Dataset):
    def __init__(self, datasetType, data_slice_id=0, num_slices=1, data_size=-1):
        if datasetType=="train":
            self.begin, self.length = 0, 6000
        elif datasetType=='test':
            self.begin, self.length = 6000, 2000
        elif datasetType=='apply':
            self.begin, self.length = 8000, 2000
        
        # To make different GPU load different data
        self.length = int(self.length / args.world_size)
        self.begin = self.begin + args.rank * self.length

        self.data = torchvision.datasets.CIFAR10(
        root="/hpcfs/cepc/higgsgpu/mo_cen/data",
        train=True,
        download=True,
        transform=transforms.Compose(
            [
                # transforms.RandomCrop(32, padding=4),
                # transforms.RandomHorizontalFlip(),
                transforms.ToTensor(),
                # transforms.Normalize(
                #     (0.4914, 0.4822, 0.4465), (0.2023, 0.1994, 0.2010)
                # ),
            ]
        ),
        )

    def __getitem__(self, index):
        return self.data[self.begin+index]

    def __len__(self):
        return self.length
