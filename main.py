import fileinput
import gc
import os
import pathlib
import argparse
import torch
import torch.nn as nn
import numpy as np
import torch
import random
import json
import torch.distributed as dist
from torch.nn.parallel import DistributedDataParallel as DDP
import copy
import time

import config
from config import args
import myDataset
from utils.confusionMatrix import get_confusion_matrix
import utils.draw as draw

if __name__ == '__main__':
    # Initialization is completed in config.py
    timeProgramStart = time.time()
    if args.distributed:
        import train_test_parallel as train_test
    else:
        import train_test

    #Find device
    device = args.device
    
    # Initialize network and result log
    net = copy.deepcopy(args.net)
    res = args.reslog.copy()
    epoch_start = 1

    if args.pre_train != 0:
        # if args.distributed:
        #     # net.load_state_dict({k.replace('module.', ''): v for k, v in                 
        #     #            torch.load(args.pre_net).items()})
        #     net.load_state_dict(torch.load(args.pre_net, map_location=device))
        # else:
        #     net.load_state_dict(torch.load(args.pre_net, map_location=device))
        net.load_state_dict(torch.load(args.pre_net, map_location=device))
            
        with open(args.pre_log,'r') as f:
            res = json.load(f)
        epoch_start = res['epochs'][-1]+1
    
    net.to(device)
    if args.distributed:
        net = DDP(net, device_ids=[args.local_rank], output_device=args.local_rank, find_unused_parameters=True)

    # Loss function 
    criterion = args.criterion

    # Record the smallest loss epoch
    bestLoss = 1e10
    bestEpoch = 0

    if args.apply_only == 0:
        # Loop epoch 
        for epoch in range(epoch_start, epoch_start + args.num_epochs):
            # Optimizer
            optimizer = args.optimizer(net.parameters(), lr=args.lr/pow(2,epoch-1)) # Fix Me! Define your own learning rate

            # Record test score
            testscores = np.zeros(0)

            # Loop slices of data
            meanLoss, meanAcc = 0, 0
            for data_slice_id in range(args.num_slices_train):
                trainloader = myDataset.get_dataloader('train',data_slice_id, args.num_slices_train, args.data_size, args.batch_size)                
                # Log epoch and lr
                res['epochs'].append(epoch)
                res['lr'].append(optimizer.param_groups[0]['lr'])

                # Train
                tes['train_slice'].append(data_slice_id)
                train_test.train_one_epoch(net, trainloader, criterion, optimizer, res)

                # Time usage
                if args.rank==0:
                    if (data_slice_id+1) in [int(i/4*args.num_slices_train) for i in range(1,5)]:
                        print("Epoch %d/%d, data_slice %d/%d training finished in %.2f min. Total time used: %.2f min." \
                                % (epoch, args.num_epochs, data_slice_id+1, args.num_slices_train,\
                                    (res['train_time'][-1])/60, (time.time() - timeProgramStart)/60), flush=True)
                del trainloader
                
            # Test
            for data_slice_id in range(args.num_slices_test):
                tes['test_slice'].append(data_slice_id)

                testloader = myDataset.get_dataloader('test',data_slice_id, args.num_slices_test, args.data_size, args.batch_size)
                ifcheckOutput = ( args.rank==0 and data_slice_id==args.num_slices_test-1 )
                scores_tmp = train_test.test_one_epoch(net, testloader, criterion, res, check_output = ifcheckOutput)

                if len(testscores) == 0:
                    testscores = scores_tmp
                else:
                    testscores = np.concatenate((testscores, scores_tmp))

                meanLoss = meanLoss + res['test_loss'][-1]
                meanAcc = meanAcc + res['test_acc'][-1]
                # Save result
                if args.rank==0:
                    json_object = json.dumps(res, indent=4)
                    with open(f"{args.logDir}/train-result.json", "w") as outfile:
                        outfile.write(json_object)
                del testloader

            meanLoss = meanLoss / args.num_slices_test
            meanAcc = meanAcc / args.num_slices_test
            if args.rank==0:
                print(f"Test: epoch: {epoch}/{epoch_start+args.num_epochs-1}, mean loss: {meanLoss}, mean acc: {meanAcc}")
                print("Total time used: %.2f min.\n"%((time.time() - timeProgramStart)/60))
                # Get confusion matrix
                cm = get_confusion_matrix(testscores)
                print("Test Confusion matrix:")
                for truth in cm:
                    print(truth/truth.sum())
                print("\n", flush=True)
                draw.draw_loss_acc(res, args.logDir)

                # Record the best epoch
                if meanLoss < bestLoss:
                    bestLoss = meanLoss
                    bestEpoch = epoch
                    # save scores given by network during test
                    np.save(args.logDir+f'/scoreTest_GPU{args.rank}.npy', arr=testscores)
                    if args.distributed:
                        torch.save(net.module.state_dict(), f'{args.logDir}/net.pt')
                    else:
                        torch.save(net.state_dict(), f'{args.logDir}/net.pt')

                print(f"Best epoch: {bestEpoch} with loss {bestLoss}")
                print("\n\n")
            if args.distributed:
                dist.barrier()

    # Apply
    net = copy.deepcopy(args.net)
    if args.distributed:
    # Use a barrier() to make sure that process 1 loads the model after process 0 saves it.
        dist.barrier()
        net.load_state_dict(torch.load(f'{args.logDir}/net.pt', map_location=device))
        net.to(device)
        net = DDP(net, device_ids=[args.local_rank], output_device=args.local_rank, find_unused_parameters=True)
    else:
        net.load_state_dict(torch.load(f'{args.logDir}/net.pt', map_location=device))
        net.to(device)
        
    applyRes = args.reslog.copy()
    scores = np.zeros(0)
    for data_slice_id in range(args.num_slices_apply):
        applyloader = myDataset.get_dataloader('apply',data_slice_id, args.num_slices_test, args.data_size, args.batch_size)
             
        scores_tmp = train_test.test_one_epoch(net, applyloader, criterion, applyRes)
        if len(scores) == 0:
            scores = scores_tmp
        else:
            scores = np.concatenate((scores, scores_tmp))

    # save scores given by network during applying
    np.save(args.logDir+f'/scoreApply_GPU{args.rank}.npy', arr=scores)
    if args.rank==0:
        print("\n\n")
        print("Apply finished.")
        print("Apply time %.2f min" % (sum(applyRes['test_time'])/60.))
        print("Apply loss: %.4f \t Apply acc: %.4f" % (sum(applyRes['test_loss'])/len(applyRes['test_loss']),
                                                        sum(applyRes['test_acc'])/len(applyRes['test_acc'])))
        print()


        # Get confusion matrix
        cm = get_confusion_matrix(scores)
        print("Confusion matrix:")
        print(cm)
        print("\n")

        print("ratio: ")
        for truth in cm:
            print(truth/truth.sum())
        
        print("\nTotal time used: %.2f min.\n"%((time.time() - timeProgramStart)/60))
        
        # Save confusion matrix
        np.save(args.logDir+f'/confusion_matrix.npy', arr=cm)

