#! /bin/bash

source ~/conda.env

# Configure input data 
filePrefix="/hpcfs/cepc/higgsgpu/mo_cen/llp/data/timeResolution/"
fileList=( $filePrefix"signal2JZbjbjWorkspace_tau_10.root"
           $filePrefix"signal2JZvvWorkspace_tau_10.root"
           $filePrefix"signal4JZbjbjWorkspace_tau_10.root"
           $filePrefix"signal4JZvvWorkspace_tau_10.root"
        )
labelList=(0 1 2 3)

nChannels=3
nClasses=10

data_size=100000
num_epochs=2
split_n_train=10
split_n_test=5
split_n_apply=1

# Apply only
apply_only=0

# Pre-train
pre_train=0
pre_net="./net.pt"
pre_log="./train-result.json"

nGPU=1
torchrun --nproc_per_node=$nGPU --master_port 25001 cnn/main.py --fileList ${fileList[@]} \
                --data_size $data_size --split_n_train $split_n_train --split_n_test $split_n_test \
                --split_n_apply $split_n_apply --num_classes $nClasses --num_channels $nChannels \
                --apply_only $apply_only  --pre_train $pre_train  --pre_net $pre_net  --pre_log $pre_log --num_epochs=$num_epochs
