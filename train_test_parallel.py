import time
import torch
from torch import nn
import torchvision.transforms as transforms
import torch.optim as optim
import numpy as np
import torch.distributed as dist
from config import args
from torch.distributed.algorithms.join import Join

device = args.device
def train_one_epoch(model, trainloader, criterion, optimizer, res):
    if args.distributed:
        dist.barrier()
    model.train()
    
    timeBegin = time.time()
    total_n_data, total_correct, total_loss = torch.tensor(0.).to(device),\
                        torch.tensor(0.).to(device), torch.tensor(0.).to(device)
                        
    with Join([model]):
        for i, data in enumerate(trainloader, 0):
            # clear the gradients of all optimized variables
            optimizer.zero_grad()

            # Model output
            inputs, labels = data
            inputs, labels = inputs.to(device), labels.to(device)
            output = model(inputs)

            # calculate the batch loss
            loss = criterion(output, labels)
            
            # backward pass: compute gradient of the loss with respect to model parameters
            loss.backward()
            optimizer.step()
            
            # Calculate Accuracy
            output = nn.Softmax(dim=1)(output)
            _, predict = torch.max(output.detach(),1)
            correct = (predict==labels).sum().item()
            
            total_n_data += len(output)
            total_correct += correct
            total_loss += loss.item()

    # record result
    if args.distributed:
        dist.barrier()
        for ary in [total_n_data, total_correct, total_loss]:   # collect data from all gpus
            dist.all_reduce(ary, op=dist.ReduceOp.SUM)      
    res['train_time'].append(time.time()-timeBegin)
    res['train_loss'].append(float(total_loss/total_n_data))
    res['train_acc'].append(float(total_correct/total_n_data))
    


def test_one_epoch(model, testloader, criterion, res, check_output = False):
    if args.distributed:
        dist.barrier()
    model.eval()
    timeBegin = time.time()
    # Record scores
    scoreLog = []

    total_n_data, total_correct, total_loss = 0, 0, 0  
    total_n_data, total_correct, total_loss = torch.tensor(0.).to(device),\
                        torch.tensor(0.).to(device), torch.tensor(0.).to(device)
    # with Join([model]):
    for i, data in enumerate(testloader, 0):
        inputs, labels = data
        inputs, labels = inputs.to(device), labels.to(device)
        with torch.no_grad():
            # forward pass: compute predicted outputs by passing inputs to the model
            output = model(inputs)

            # calculate the batch loss
            loss = criterion(output, labels)

            # Calculate Accuracy
            output = nn.Softmax(dim=1)(output)
            _, predict = torch.max(output.detach(),1)
            correct = (predict==labels).sum().item()

            total_n_data += len(output)
            total_correct += correct
            total_loss += loss.item()
            scoreLog.append(torch.cat((labels.reshape(-1,1),output), 1).detach().cpu().numpy() )

    if check_output:
        print(" labels: ", labels[0:5])
        print(" output: ", output[0:5, :])
        print()

    # record result
    scores = np.zeros((0,scoreLog[0].shape[1]))
    for s in scoreLog:
        scores = np.concatenate( (scores, s), 0)
    
    del scoreLog
    
    if args.distributed:
        dist.barrier()
        for ary in [total_n_data, total_correct, total_loss]:   # collect data from all gpus
            dist.all_reduce(ary, op=dist.ReduceOp.SUM)    
              
    res['test_time'].append(time.time()-timeBegin)
    res['test_loss'].append(float(total_loss/total_n_data))
    res['test_acc'].append(float(total_correct/total_n_data))

    return scores
